/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.titima.midterm;

/**
 *
 * @author acer
 */
public class Product {
    private String id;
    private String name;
    private String brand;
    private double Price;
    private int amount;

    public Product(String id, String name, String brand, double Price, int amount) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.Price = Price;
        this.amount = amount;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product " + "Id = " + id + ", Name = " + name + ", Brand = " + brand + ", Price = " + Price + ", Amount = " + amount ;
    }
    
}
